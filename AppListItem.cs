//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Backend
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppListItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<bool> isChecked { get; set; }
        public Nullable<int> Amount { get; set; }
        public Nullable<int> Type { get; set; }
        public string Notes { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> ListID { get; set; }
        public int Icon { get; set; }
        public string defaultName { get; set; }
    
        public virtual AppList AppList { get; set; }
    }
}
