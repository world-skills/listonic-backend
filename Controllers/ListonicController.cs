﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Backend.Controllers
{
    public class ListonicController : ApiController
    {
        private ListonicEntities db = new ListonicEntities();

        [HttpPut]
        [Route("login")]
        public IHttpActionResult Login([System.Web.Http.FromBody]AppUser request)
        {
            byte[] result = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(request.Password));

            var hash = BitConverter.ToString(result);

            var existingUser = db.AppUsers.Where(it => it.Email == request.Email && it.Password == hash).
                Select(it => new { it.ID, it.FirstName, it.LastName }).FirstOrDefault();

            if (existingUser == null)
            {
                return NotFound();
            }

            return Json(existingUser);
        }

        [HttpPut]
        [Route("register")]
        public IHttpActionResult Register(AppUser request)
        {
            byte[] result = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(request.Password));


            request.Password = BitConverter.ToString(result);
            db.AppUsers.Add(request);

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return NotFound();
            }

            return Ok(request);
        }

        [HttpGet]
        [Route("get-all-lists")]
        public IHttpActionResult GetAllLists([FromUri]int id)
        {
            var lists = db.AppLists.Where(it => it.UserID == id && it.inTrash != true)
                .Select(it => new
                {
                    it.ID,
                    it.Name,
                    it.inTrash,
                    it.DeletedSince,
                    AppListItems = it.
                    AppListItems.
                    Select(item =>
                    new { item.ID, item.Name, item.isChecked, item.Notes, item.Price, item.ListID, item.Amount, item.Icon, item.defaultName })
                }).ToList();

            return Json(lists);
        }

        [HttpGet]
        [Route("get-trash-lists")]
        public IHttpActionResult GetTrashLists([FromUri]int id)
        {
            var lists = db.AppLists.Where(it => it.UserID == id && it.inTrash == true)
                .Select(it => new
                {
                    it.ID,
                    it.Name,
                    it.inTrash,
                    it.DeletedSince,
                    AppListItems = it.
                    AppListItems.
                    Select(item =>
                    new { item.ID, item.Name, item.isChecked, item.Notes, item.Price, item.ListID, item.Amount, item.Icon, item.defaultName })
                }).ToList();

            return Json(lists);
        }

        [HttpPut]
        [Route("add-new-list")]
        public IHttpActionResult AddNewList([FromBody]AppList list)
        {

            db.AppLists.Add(list);

            list.AppListItems.ToList().ForEach(it => db.AppListItems.Add(it));



            db.SaveChanges();

            var newList = new
            {
                list.ID,
                list.Name,
                list.UserID,
                list.inTrash,
                list.DeletedSince,
                AppListItems = list.AppListItems.Select(it => new
                {
                    it.ID,
                    it.Icon,
                    it.isChecked,
                    it.defaultName,
                    it.ListID,
                    it.Name,
                    it.Type,
                    it.Notes,
                    it.Amount,
                    it.Price
                })
            };



            return Ok(newList);
        }

        [HttpPut]
        [Route("update-list")]
        public IHttpActionResult UpdateList([FromBody]AppList list)
        {
            list.AppListItems.ToList().ForEach(item =>
            {
                var result = db.AppLists.SingleOrDefault(it => it.ID == list.ID);

                var oldItem = result.AppListItems.FirstOrDefault(it => it.defaultName == item.defaultName);

                if (oldItem != null)
                {
                    oldItem.isChecked = item.isChecked;
                    oldItem.Amount = item.Amount;
                    oldItem.Name = item.Name;
                    oldItem.Notes = item.Notes;
                    oldItem.Price = item.Price;
                    if (oldItem.Amount <= 0)
                    {
                        db.AppListItems.Remove(oldItem);
                    }
                }
                else
                {
                    result.AppListItems.Add(item);
                }
            });

            db.AppLists.FirstOrDefault(it => it.ID == list.ID).Name = list.Name;

            db.SaveChanges();


            return Ok(list);
        }

        [HttpPut]
        [Route("move-to-trash")]
        public IHttpActionResult MoveListToTrash([FromBody] AppList req)
        {
            var list = db.AppLists.Where(it => it.ID == req.ID).FirstOrDefault();
            list.inTrash = true;
            db.SaveChanges();

            return Ok();
        }

        [HttpPut]
        [Route("restore-list")]
        public IHttpActionResult RestoreList(int listID)
        {
            var list = db.AppLists.Where(it => it.ID == listID).FirstOrDefault();
            list.inTrash = false;
            db.SaveChanges();

            return Ok();
        }

        [HttpPut]
        [Route("delete-list")]
        public IHttpActionResult DeleteList(int listID)
        {
            var list = db.AppLists.Where(it => it.ID == listID).FirstOrDefault();

            if (list == null)
            {
                return NotFound();
            }

            list.AppListItems.ToList().ForEach(item => db.AppListItems.Remove(item));

            db.AppLists.Remove(list);
            db.SaveChanges();

            return Ok();
        }

        [HttpPut]
        [Route("change-item-status")]
        public IHttpActionResult ChangeItemStatus(int itemID)
        {
            var item = db.AppListItems.Where(it => it.ID == itemID).FirstOrDefault();
            item.isChecked = !item.isChecked;
            db.SaveChanges();

            return Ok();
        }

        [HttpPut]
        [Route("update-item-info")]
        public IHttpActionResult UpdateItemInfo([System.Web.Http.FromBody] AppListItem item)
        {
            var dbItem = db.AppListItems.Where(it => it.ID == item.ID).FirstOrDefault();
            dbItem.Name = item.Name;
            dbItem.Notes = item.Notes;
            dbItem.Price = item.Price;

            db.SaveChanges();

            return Ok();
        }
    }
}

    
